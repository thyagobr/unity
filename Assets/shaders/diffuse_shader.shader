﻿Shader ".sh4dz/diffuse_shader"
{
	Properties
	{
		_Color("Main Color", Color) = (.8, 0, .7, 1)
		_MainTex("Main Texture", 2D) = "white" {}
	}

		SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			fixed4 _Color;
			float4 _LightColor0;
			sampler2D _MainTex;

			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
			};

			v2f vert(appdata IN)
			{
				v2f OUT;
				OUT.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.normal = mul(float4(IN.normal, 0.0), _Object2World).xyz;
				OUT.texcoord = IN.texcoord;
				return OUT;
			}

			fixed4 frag(v2f IN) : COLOR
			{
				fixed4 texcolor = tex2D(_MainTex, IN.texcoord);

			float3 normal_direction = normalize(IN.normal);
			float3 light_direction = normalize(_WorldSpaceLightPos0.xyz);
			float3 diffuse = _LightColor0.rgb * max(0.0, dot(normal_direction, light_direction));
			return _Color * texcolor * float4(diffuse, 1);
				//return texcolor;
				// return _Color;
		}

		ENDCG
	}
	}
}
