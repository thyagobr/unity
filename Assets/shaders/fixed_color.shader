﻿Shader ".sh4dz/fixed_color"
{
	Properties
	{
		_Color("Main Color", Color) = (.8, 0, .7, 1)
		_MainTex("Main Texture", 2D) = "white" {}
	}

		SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			fixed4 _Color;
			sampler2D _MainTex;

			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 texcoord : TEXCOORD0;
			};

			v2f vert(appdata IN)
			{
				v2f OUT;
				OUT.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				return OUT;
			}

			fixed4 frag(v2f IN) : COLOR
			{
				fixed4 texcolor = tex2D(_MainTex, IN.texcoord);
				return texcolor;
				// return _Color;
		}

		ENDCG
	}
	}
}
